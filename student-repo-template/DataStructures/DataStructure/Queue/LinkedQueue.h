#ifndef LINKED_QUEUE_HPP
#define LINKED_QUEUE_HPP

// Project includes
#include "../LinkedList/LinkedList.h"
#include "../../Utilities/Logger.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

template <typename T>
//! A first-in-first-out (FIFO) queue structure built on top of a linked list
class LinkedQueue
{
public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop();
    //! Access the data at the front of the queue
    T Front();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

private:
    LinkedList<T> m_list;

    friend class QueueTester;
};

template <typename T>
void LinkedQueue<T>::Push(const T& newData )
{
    throw Exception::NotImplementedException( "ArrayQueue::Push is not implemented" );
}

template <typename T>
void LinkedQueue<T>::Pop()
{
    throw Exception::NotImplementedException( "ArrayQueue::Pop is not implemented" );
}

template <typename T>
T LinkedQueue<T>::Front()
{
    throw Exception::NotImplementedException( "ArrayQueue::Front is not implemented" );
}

template <typename T>
int LinkedQueue<T>::Size()
{
    throw Exception::NotImplementedException( "ArrayQueue::Size is not implemented" );
}

template <typename T>
bool LinkedQueue<T>::IsEmpty()
{
    throw Exception::NotImplementedException( "ArrayQueue::IsEmpty is not implemented" );
}

} // End of namespace

#endif
