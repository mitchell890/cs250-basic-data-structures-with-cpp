#ifndef _DATA_STRUCTURES_PROGRAM
#define _DATA_STRUCTURES_PROGRAM

// C++ Library includes
#include <string>

//! Holds menus for this program
class DataStructuresProgram
{
    public:
    DataStructuresProgram();

    void MainMenu();

    void Menu_LinkedList();
    void Menu_Stack();
    void Menu_Queue();
    void Menu_BinarySearchTree();
    void Menu_HashTable();
    void Menu_Projects();

    void SetWebBrowser( std::string browser );
    void SetAutoOpenTestResults( bool value );
    void OpenResults( std::string path );

    private:
    std::string m_webbrowser;
    bool m_autoOpenTestResults;
};

#endif
