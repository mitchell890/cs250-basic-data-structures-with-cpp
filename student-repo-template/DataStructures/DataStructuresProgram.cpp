// C++ Library includes
#include <cstdlib>

// Project includes
#include "DataStructuresProgram.h"
#include "Utilities/Menu.h"
#include "DataStructure/LinkedList/LinkedListTester.h"
#include "DataStructure/Stack/StackTester.h"
#include "DataStructure/Queue/QueueTester.h"
#include "DataStructure/BinarySearchTree/BinarySearchTreeTester.h"
#include "DataStructure/HashTable/HashTableTester.h"


DataStructuresProgram::DataStructuresProgram()
{
    m_webbrowser = "firefox";
    m_autoOpenTestResults = true;
}

//! The main menu of the program; shows a list of options of things we can run.
void DataStructuresProgram::MainMenu()
{
    bool done = false;
    while ( !done )
    {
        Utility::Menu::Header( "MAIN MENU" );

        std::string choice = Utility::Menu::ShowStringMenuWithPrompt( {
            "Run [Linked List] Unit Tests",
            "Run [Stack Unit] Tests",
            "Run [Queue Unit] Tests",
            "Run [Binary Search] Tree Unit Tests",
            "Run [Hash Table] Unit Tests",
            "Projects",
            "Exit"
        } );

        if      ( choice == "Run [Linked List] Unit Tests" )         { Menu_LinkedList(); }
        else if ( choice == "Run [Stack Unit] Tests" )               { Menu_Stack(); }
        else if ( choice == "Run [Queue Unit] Tests" )               { Menu_Queue(); }
        else if ( choice == "Run [Binary Search] Tree Unit Tests" )  { Menu_BinarySearchTree(); }
        else if ( choice == "Run [Hash Table] Unit Tests" )          { Menu_HashTable(); }
        else if ( choice == "Projects" )                             { Menu_Projects(); }
        else if ( choice == "Exit" )                                 { done = true; }
        else                                                         { std::cout << "** Command not recognized!" << std::endl; }
    }
}

void DataStructuresProgram::Menu_LinkedList()
{
    Utility::Menu::Header( "RUN [LINKED LIST] UNIT TESTS" );
    DataStructure::LinkedListTester tester;
    tester.Start();
    OpenResults( "test_result_linked_list.html" );
}

void DataStructuresProgram::Menu_Stack()
{
    Utility::Menu::Header( "RUN [STACK] UNIT TESTS" );
    DataStructure::StackTester tester;
    tester.Start();
    OpenResults( "test_result_stack.html" );
}

void DataStructuresProgram::Menu_Queue()
{
    Utility::Menu::Header( "RUN [QUEUE] UNIT TESTS" );
    DataStructure::QueueTester tester;
    tester.Start();
    OpenResults( "test_result_queue.html" );
}

void DataStructuresProgram::Menu_BinarySearchTree()
{
    Utility::Menu::Header( "RUN [BINARY SEARCH TREE] UNIT TESTS" );
    DataStructure::BinarySearchTreeTester tester;
    tester.Start();
    OpenResults( "test_result_binary_search_tree.html" );
}

void DataStructuresProgram::Menu_HashTable()
{
    Utility::Menu::Header( "RUN [HASH TABLE] UNIT TESTS" );
    DataStructure::HashTableTester tester;
    tester.Start();
    OpenResults( "test_result_hash_table.html" );
}

//! Show menu of available Project sub-programs
void DataStructuresProgram::Menu_Projects()
{
    Utility::Menu::Header( "PROJECTS" );

    std::cout << "No projects available yet" << std::endl;
}

void DataStructuresProgram::SetWebBrowser( std::string browser )
{
    m_webbrowser = browser;
}

void DataStructuresProgram::SetAutoOpenTestResults( bool value )
{
    m_autoOpenTestResults = value;
}

//! Attempts to open the unit test results in a web browser
void DataStructuresProgram::OpenResults( std::string path )
{
    if ( !m_autoOpenTestResults ) { return; }
    std::string command = m_webbrowser + " " + path;
    system( command.c_str() );
}
