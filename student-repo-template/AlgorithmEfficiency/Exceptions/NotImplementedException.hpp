#ifndef _NOT_IMPLEMENTED_EXCEPTION
#define _NOT_IMPLEMENTED_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

//! EXCEPTION for when a function has not yet been implemented
class NotImplementedException : public runtime_error
{
    public:
    NotImplementedException( string functionName )
        : runtime_error( functionName + " is not yet implemented! Make sure to remove the \"throw NotImplementedException\" when working on a function." ) { ; }
};

#endif
